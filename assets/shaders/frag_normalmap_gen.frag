#version 330

/**
 This shader generates a normalmap from a displacement map
 Applies the sobel operator in 2 direction x and y

 x:            y:
 +1 0 -1       -1 -2 -1
 +2 0 -2        0  0  0
 +1 0 -1       +1 +2 +1

 After that compute the "z" component using the Pytagoras' rule: nz = sqrt(1 - nx*nx - ny*ny)
**/

// uniforms
uniform sampler2D dispTex;	// displacement map texture
uniform int width;			// texture dimensions
uniform int height;
uniform float dispScale;	// displacement scale

// out
out vec4 outColor;

// varyings
in vec3 varPos;
in vec2 varTexCoord;

void main()
{
	vec2 tc = varTexCoord;
	float tw = 1.0 / width;		// texel width
	float th = 1.0 / height;	// texel height
	
	float topLeft  = texture(dispTex, tc + vec2(-tw, +th)).r * dispScale;
	float topMid   = texture(dispTex, tc + vec2(  0, +th)).r * dispScale;
	float topRight = texture(dispTex, tc + vec2(+tw, +th)).r * dispScale;
	float midLeft  = texture(dispTex, tc + vec2(-tw,   0)).r * dispScale;
	float midMid   = texture(dispTex, tc + vec2(  0,   0)).r * dispScale;
	float midRight = texture(dispTex, tc + vec2(+tw,   0)).r * dispScale;
	float botLeft  = texture(dispTex, tc + vec2(-tw, -th)).r * dispScale;
	float botMid   = texture(dispTex, tc + vec2(  0, -th)).r * dispScale;
	float botRight = texture(dispTex, tc + vec2(+tw, -th)).r * dispScale;

	
	float nx =
		+ topLeft - topRight
		+ 2 * midLeft - 2 * midRight
		+ botLeft - botRight;
		
	float ny =
		+ botLeft - topLeft
		+ 2 * botMid - 2 * topMid
		+ botRight - topRight;
	
	float nz = 1;
	
	vec3 N = normalize(vec3(nx, ny, nz));
	
	outColor = vec4(0.5 * N + vec3(0.5), 1);
	//outColor = vec4(varPos.xy, 0, 1);
	//outColor = vec4(midMid, 0, 0, 1);
}