#version 330 core

in vec3 inPos;

uniform mat4 model;
uniform mat4 modelViewProj;

uniform vec4 waterPlane;

out vec3 varPos;

void main()
{
	varPos = vec3(modelViewProj * vec4 (inPos,1.0));
	
	gl_Position =  modelViewProj * vec4 (inPos,1.0);
	gl_ClipDistance[0] = dot(waterPlane, model * vec4(inPos, 1.0));
}
