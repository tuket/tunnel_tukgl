#version 330 core

// uniforms
uniform uint shadingModelId;
uniform sampler2D colorTex;
uniform sampler2D normalTex;
uniform sampler2D customTex;

// out
out vec4 gtex_albedoAndGloss;
out vec4 gtex_specularAndShadingModelId;
out vec4 gtex_emi;
out vec4 gtex_normal;

// varyings
in vec3 varPos;
in float varDisp;
in vec2 varTexCoord;
in vec3 varNormal;
in vec3 varTangent;

void main()
{
	vec4 color = texture(colorTex, varTexCoord);
	gtex_albedoAndGloss = color;
	gtex_albedoAndGloss.a = 8 / 255.0;
	
	gtex_emi = 0.2*pow(varDisp/1.5, 0.6) * color;
	
	vec3 normal = normalize(varNormal);
	vec3 tangent = normalize(varTangent);
	vec3 bitangent = cross(normal, tangent);
	mat3 TBN = mat3(tangent, bitangent, normal);

	vec3 N = normalize(2 * texture(normalTex, varTexCoord).xyz - vec3(1.0));;
	N = TBN * N;

	gtex_normal = vec4(vec3(0.5, 0.5, 0.5) + 0.5 * N, 0.0);
	
	gtex_specularAndShadingModelId.xyz = vec3(0.1, 0.1, 0.1);
	
	gtex_specularAndShadingModelId.w = float(shadingModelId) / 255.0;
}
