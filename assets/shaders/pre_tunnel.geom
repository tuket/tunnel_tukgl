#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices=256) out;

uniform mat4 model;
uniform mat4 modelViewProj;
uniform mat4 normal;
uniform sampler2D customTex;	//< displacement map
uniform float displacementFactor;
uniform vec4 waterPlane;
//uniform float time;

in vec3 vertPos[];
in vec3 vertNormal[];
in vec3 vertTangent[];
in vec2 vertTexCoord[];

out vec3 varPos;
out float varDisp;
out vec2 varTexCoord;
out vec3 varNormal;
out vec3 varTangent;

vec3 getNormal(vec3 A, vec3 B, vec3 C)
{
   vec3 a = A - B;
   vec3 b = C - B;
   return -normalize(cross(a, b));
}

const float PI = 3.14159;

void main()
{
	
	vec3 p1 = gl_in[0].gl_Position.xyz;
	vec3 p2 = gl_in[1].gl_Position.xyz;
	vec3 p3 = gl_in[2].gl_Position.xyz;
		
	float disp1 = displacementFactor * texture(customTex, vertTexCoord[0]).r;
	float disp2 = displacementFactor * texture(customTex, vertTexCoord[1]).r;
	float disp3 = displacementFactor * texture(customTex, vertTexCoord[2]).r;
	p1 += disp1 * vertNormal[0];
	p2 += disp2 * vertNormal[1];
	p3 += disp3 * vertNormal[2];

	vec3 triNormal = getNormal(p1, p2, p3);
	
	varDisp = disp1;
	varTexCoord = vertTexCoord[0];
	varNormal = (normal * vec4(vertNormal[0], 0)).xyz;
	varTangent = (normal * vec4(vertTangent[0], 0)).xyz;
	gl_Position = modelViewProj * vec4(p1, 1.0);
	gl_ClipDistance[0] = dot(waterPlane, model * vec4(p1, 1.0));
	EmitVertex();
	
	varDisp = disp2;
	varTexCoord = vertTexCoord[1];
	varNormal = (normal * vec4(vertNormal[1], 0)).xyz;
	varTangent = (normal * vec4(vertTangent[1], 0)).xyz;
	gl_Position = modelViewProj * vec4(p2, 1.0);
	gl_ClipDistance[0] = dot(waterPlane, model * vec4(p2, 1.0));
	EmitVertex();
	
	varDisp = disp3;
	varTexCoord = vertTexCoord[2];
	varNormal = (normal * vec4(vertNormal[2], 0)).xyz;
	varTangent = (normal * vec4(vertTangent[2], 0)).xyz;
	gl_Position = modelViewProj * vec4(p3, 1.0);
	gl_ClipDistance[0] = dot(waterPlane, model * vec4(p3, 1.0));
	EmitVertex();

	EndPrimitive();
	
}