#version 330 core

in vec3 inPos;
in vec3 inNormal;
in vec3 inTangent;
in vec2 inTexCoord;

out vec3 vertPos;
out vec3 vertNormal;
out vec3 vertTangent;
out vec2 vertTexCoord;

void main()
{
	vertNormal = normalize(inNormal);
	vertTangent = normalize(inTangent);

	vertPos = inPos;
	vertTexCoord = inTexCoord;
	gl_Position = vec4(inPos, 1.0);
}
