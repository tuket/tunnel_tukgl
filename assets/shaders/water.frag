#version 330 core

// uniforms
uniform uint shadingModelId;
uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform vec4 tint;  // the color of the water, the a component is the transparency of the water
uniform float time;

// varyings
in vec4 varPos;
in vec3 varCamToPoint;

// out
out vec4 gtex_albedoAndGloss;
out vec4 gtex_specularAndShadingModelId;
out vec4 gtex_emi;
out vec4 gtex_normal;

float fresnel(vec3 V, vec3 N, float F0)
{
    float VoN = abs(dot(V, N));
    return F0 + (1-F0) * pow(1 - VoN, 5);
}

vec2 distortion(float strength, vec2 tc, float invDepth)
{
    vec2 s1 = invDepth * vec2(sin(time + 1/invDepth * 2*tc.y), cos(time + 1/invDepth * 2*tc.x));
    vec2 s2 = invDepth * vec2(sin(16.424 + 2 * time + 1/invDepth * 4*tc.y), cos(21.234 + 2*time + 1/invDepth * 4*tc.x));
    vec2 s3 = invDepth * vec2(sin(4.974 + 4 * time + 1/invDepth * 8*tc.y), cos(34.943 + 4*time + 1/invDepth * 8*tc.x));

    vec2 total = s1 + s2 + s3;
    return total * strength;
}

void main()
{

    vec2 ndc = varPos.xy / varPos.w;
    vec2 tc = 0.5 * ndc + vec2(0.5);
    float invDepth = 1 / length(varCamToPoint);
    tc += distortion(0.01, tc, invDepth);
    vec3 reflectionColor = texture(emiTex, vec2(tc.x, -tc.y)).rgb;
    vec3 refractionColor = texture(colorTex, tc).rgb;

    // add tint to refraction
    refractionColor = mix(refractionColor, tint.rgb, tint.a);

    vec3 camToPoint = normalize(varCamToPoint);
    float blendFactor = 1 - fresnel(camToPoint, vec3(0, 1, 0), 0.25);
    vec3 color = mix(reflectionColor, refractionColor, blendFactor);
    
    gtex_emi.rgb = color;

    gtex_specularAndShadingModelId.w = float(shadingModelId) / 255.0;
}
