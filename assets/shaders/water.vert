#version 330 core

in vec3 inPos;
in vec3 inColor;
in vec2 inTexCoord;
in vec3 inNormal;
in vec3 inTangent;

uniform mat4 modelViewProj;
uniform mat4 model;
uniform mat4 normal;
uniform vec3 camPos;

out vec4 varPos;
out vec3 varCamToPoint;

void main()
{
	varPos = modelViewProj * vec4 (inPos,1.0);
    varCamToPoint = (model * vec4(inPos, 1.0)).xyz - camPos;
	gl_Position =  varPos;
}
