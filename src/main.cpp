#include "tukgl/util/auxiliar.hpp"

#include <SDL.h>
#include <glad/glad.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include "tukgl/render/lights.hpp"
#include <algorithm>
#include "tukgl/render/renderer.hpp"
#include "tukgl/shader/shader_pool.hpp"
#include "tukgl/scene/skybox.hpp"
#include "tukgl/mesh/mesh_pool.hpp"
#include "tukgl/mesh/spline_tube.hpp"
#include "tukgl/util/splines.hpp"
#include "tukgl/mesh/cylinder.hpp"
#include "tukgl/texture/texture.hpp"
#include "tukgl/texture/render_target.hpp"
#include "tukgl/scene/go_camera.hpp"
#include "tukgl/mesh/plane.hpp"

using namespace std;
using namespace glm;

// Constants
const unsigned SCREEN_WIDTH = 800;
const unsigned SCREEN_HEIGHT = 600;
const float NEAR_PLANE_DIST = 0.1f;
const float FAR_PLANE_DIST = 500.f;
const float FOV_Y = 60;
const float CAMERA_SPEED = 12.f;
//const float CAMERA_SPEED = 30.f;
const float DISPLACEMENT_FACTOR = 6.f;
//water
const float WATER_HEIGHT = -8.f;
const vec4 WATER_TINT = vec4(0, 0.5, 0.8, 0.05);

// varaibles
static SDL_Window* window;
static bool run;
static float globalTime;
static float splineTime;

static Renderer* renderer;
static Scene* scene;
static SceneNode *tunnelNode, *pointLightNode, *waterPlaneNode;
vector<SceneNode*> plants;
static Catmull* spline;
static RenderTarget reflectionTarget, refractionTarget;

// wasd keys status
static bool wPressed = false, aPressed = false, sPressed = false, dPressed = false;
static bool automaticCamera = true;
// mouse status
bool mousePressed = false;
int prevMouseX, prevMouseY;

float cameraHeading = 0, cameraPitch = 0;

// TODEL
TextureId voro;

// updater functions
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc(float dt);
void handleEvent(const SDL_Event& event);
void updateKeyboardState();
void updateMouseState();
void mouseMotionFunc(int x, int y);

void cameraMovement(float dt);

// init functions
void initContext(int argc, char** argv);
void initOGL();
void initScene();
void initWaterPlane();
void initTunnel(TextureId displacementMap, TextureId normalMap);
void initLights();
void initPlants(SceneNode* root);

int main(int argc, char** argv)
{

	cout << SDL_GetBasePath() << endl;

	initContext(argc, argv);
	initOGL();
	renderer = new Renderer();
	initScene();
	resizeFunc(SCREEN_WIDTH, SCREEN_HEIGHT);
	splineTime = 0;

	renderer->enableDof(false);
	renderer->setDofFocalDistance(5);
	renderer->setDofStrength(0);
	renderer->enableMotionBlur(false);
	renderer->setMotionBlurStrength(0.5f);

	static float prevTime = (float)SDL_GetTicks();
	static float curTime = (float)SDL_GetTicks();
	run = true;
	while (run)
	{

		// compute delta time
		curTime = (float)SDL_GetTicks();
		float dt = (curTime - prevTime) / 1000.f;
		prevTime = curTime;

		SDL_Event event;
		// handle events
		if (SDL_PollEvent(&event)) {
			handleEvent(event);
		}
		updateKeyboardState();
        updateMouseState();

		idleFunc(dt);

		renderFunc();

		globalTime += dt;
		/*
		// limit FPS
		unsigned int endTicks = SDL_GetTicks();
		int sleepTicks = 1000 / LIMIT_FPS - (endTicks - beginTicks);
		if (sleepTicks > 0)
			SDL_Delay(sleepTicks);*/
	}

	return 0;
}

void initContext(int argc, char** argv)
{
	SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_TIMER);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	// don't allow deprecated GL functions
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// make the window
	window =
			SDL_CreateWindow(
					"tracer",
					100, 100,
					SCREEN_WIDTH, SCREEN_HEIGHT,
					SDL_WINDOW_OPENGL
			);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	if (!gladLoadGL())
	{
		printf("gladLoadGL failed\n");
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;
	const GLubyte *gpuVendor = glGetString(GL_VENDOR);
	std::cout << "Graphics card: " << gpuVendor << std::endl;
}

void initOGL()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.15f, 0.15f, 0.15f, 1.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);
}

void initScene()
{
	scene = new Scene();

	SceneNode* root = new SceneNode();

	unsigned VORONOI_TEX_WIDTH = 1024;
	unsigned VORONOI_TEX_HEIGHT = 1024;
	// generate voronoi texture for the displacement map
	TexFragShader voronoiTexShad("shaders/frag_voronoi.frag");
	{
		int widthUnifLoc = voronoiTexShad.getUnifLoc("width");
		int heightUnifLoc = voronoiTexShad.getUnifLoc("height");
		int expUnifLoc = voronoiTexShad.getUnifLoc("exp");
		unsigned program = voronoiTexShad.getProgram();
		glUseProgram(program);
		glUniform1i(widthUnifLoc, 16);
		glUniform1i(heightUnifLoc, 16);
		glUniform1f(expUnifLoc, 1);
	}
	RenderTarget voronoiTarget(1, VORONOI_TEX_WIDTH, VORONOI_TEX_HEIGHT, TextureType::RGBA8);
	voronoiTarget.bind();
	renderer->renderFragShader(voronoiTexShad, voronoiTarget);
	TextureId voronoiTex = voronoiTarget.getTexture(0);
	//saveTexture("vorinot.png", voronoiTex);

	// create a normal map from the previously generated voronoi displacement map
	TexFragShader normalMapGenShad("shaders/frag_normalmap_gen.frag");
	RenderTarget voronoiNormalMapTarget(1, VORONOI_TEX_WIDTH, VORONOI_TEX_HEIGHT, TextureType::RGBA8);
	voronoiNormalMapTarget.bind();
	{
		int widthUnifLoc = normalMapGenShad.getUnifLoc("width");
		int heightUnifLoc = normalMapGenShad.getUnifLoc("height");
		int dispScaleUnifLoc = normalMapGenShad.getUnifLoc("dispScale");
		int dispTexUnifLoc = normalMapGenShad.getUnifLoc("dispTex");
		unsigned program = normalMapGenShad.getProgram();
		glUseProgram(program);
		glUniform1i(widthUnifLoc, VORONOI_TEX_WIDTH);
		glUniform1i(heightUnifLoc, VORONOI_TEX_HEIGHT);
		glUniform1f(dispScaleUnifLoc, DISPLACEMENT_FACTOR);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, voronoiTarget.getTexture(0));
		glUniform1i(dispTexUnifLoc, 0);
	}
	renderer->renderFragShader(normalMapGenShad, voronoiNormalMapTarget);
	TextureId voronoiNormalMapTex = voronoiNormalMapTarget.getTexture(0);
	voro = voronoiNormalMapTex;
	//saveTexture("vorinot_normal.png", voronoiNormalMapTex);

	// create render targets for water reflection and refraction
	{
		const unsigned WIDTH = SCREEN_WIDTH;
		const unsigned HEIGHT = SCREEN_HEIGHT;
		reflectionTarget = RenderTarget(1, WIDTH, HEIGHT, TextureType::RGBA8, false);
		refractionTarget = RenderTarget(1, WIDTH, HEIGHT, TextureType::RGBA8, true);
	}

	// add camera
	SceneNode* cameraNode = new SceneNode();
	GOCamera* camera = new GOCamera(
		nullptr, FOV_Y,
		(float)SCREEN_WIDTH / SCREEN_HEIGHT,
		NEAR_PLANE_DIST, FAR_PLANE_DIST);
	cameraNode->setGameObject(camera);

	initLights();
	initTunnel(voronoiTex, voronoiNormalMapTex);
	initWaterPlane();
	initPlants(root);

	cameraNode->attachChild(pointLightNode);
	root->attachChild(cameraNode);
	root->attachChild(tunnelNode);
	root->attachChild(waterPlaneNode);

	scene->setRoot(root);
	scene->setActiveCamera(camera);

	cameraNode->setPosition(vec3(0, 0, 50));
	cameraNode->setRotation(quat(1, 0, 0, 0));
	waterPlaneNode->setPosition(vec3(0.0, WATER_HEIGHT, 0.0));

	// skybox
	unsigned skyboxTexId = loadCubamap(
			"img/interstellar_skybox/xpos.png", "img/interstellar_skybox/xneg.png",
			"img/interstellar_skybox/yneg.png", "img/interstellar_skybox/ypos.png",
			"img/interstellar_skybox/zneg.png", "img/interstellar_skybox/zpos.png"
	);
	scene->setSkyboxCubeMap(skyboxTexId);
	scene->setSkyboxEnabled(false);

	renderer->setWireframeMode(false);

}

void initLights()
{
	// point light
	pointLightNode = new SceneNode();
	GOPointLight* pointLight = new GOPointLight();
	pointLight->setAmbientIntensity(glm::vec3(0.0));
	pointLight->setDiffuseAndSpecularIntensity(glm::vec3(500));
	pointLight->setC0(1);
	pointLight->setC1(1);
	pointLight->setC2(1);
	pointLightNode->setGameObject(pointLight);
	pointLightNode->setPosition(glm::vec3(0, 0, 0));
}

void initTunnel(TextureId displacementMap, TextureId normalMap)
{
	ShaderPool& shaderPool = ShaderPool::getInstance();
	PrePassShaderId shaderId = shaderPool.registerPrePassShader(
		"tunnel",
		"shaders/pre_tunnel.vert",
		"shaders/pre_tunnel.geom",
		"shaders/pre_tunnel.frag");

	// create material
	Material material;
	material.setShaderID(shaderId);
	material.setShadingModelId(ShadingModelId::PHONG);
	material.setColorTexture("img/ground.jpg");
	material.setNormalTextureId(normalMap);
	material.setCustomTextureId(displacementMap);
	material.setUnifValue("displacementFactor", DISPLACEMENT_FACTOR);

	// create the spline
	vector<vec2> splinePoints;
	generateRandCircularControlPoints(splinePoints, 10, 50, 0.3);
	spline = new Catmull(splinePoints, 0.8);

	// create the mesh
	Mesh mesh = SplineTube::create(*spline, 10, 128 * 2, 32 * 2, 4, true);
	MeshVAO vao;
	MeshVBOs vbos;
	uploadMesh(vao, vbos, mesh);
	MeshPool& meshPool = MeshPool::getInstance();
	meshPool.insertCustomMesh("tunnel", vao, vbos, mesh.numTriangles);
	freeMeshMemory(mesh);

	// create scene node
	tunnelNode = new SceneNode();
	GOMesh* go = new GOMesh();
	go->setMeshFromCustomName("tunnel");
	tunnelNode->setGameObject(go);
	go->setMaterial(material);
}

void initWaterPlane()
{
	// create geometry
	MeshPool& meshPool = MeshPool::getInstance();
	Mesh mesh = createPlaneMesh(
		200, 200,	// dimensions
		1, 1,		// quad resolution
		1, 1);		// texture tiling
	MeshVAO vao;
	MeshVBOs vbos;
	uploadMesh(vao, vbos, mesh);
	meshPool.insertCustomMesh("water_plane", vao, vbos, mesh.numTriangles);
	freeMeshMemory(mesh);

	// create scene node
	waterPlaneNode = new SceneNode();
	waterPlaneNode->setPosition(vec3(0, 0, 0));
	GOMesh* go = new GOMesh();
	go->setMeshFromCustomName("water_plane");
	waterPlaneNode->setGameObject(go);

	// create material
	ShaderPool& shaderPool = ShaderPool::getInstance();
	PrePassShaderId shaderId = shaderPool.registerPrePassShader(
		"water",
		"shaders/water.vert",
		"shaders/water.frag");
	Material material;
	material.setShaderID(shaderId);
	material.setShadingModelId(ShadingModelId::UNLIT);
	material.setColorTextureId(refractionTarget.getTexture(0));
	material.setEmissiveTextureId(reflectionTarget.getTexture(0));
	material.setUnifValue("tint", WATER_TINT);
	material.setUnifValue("time", 0);
	go->setMaterial(material);

}

void initPlants(SceneNode* root)
{
	int NUM_PLANTS = 6;
	// add plant
	for (int i = 0; i < NUM_PLANTS; i++)
	{
		SceneNode* node = new SceneNode();
		GOMesh* mesh = new GOMesh();
		mesh->setMeshFromFileName("meshes/plant.obj");
		node->setGameObject(mesh);
		ShaderPool& shaderPool = ShaderPool::getInstance();
		// create material
		PrePassShaderId shaderId = shaderPool.registerPrePassShader(
			"plant",
			"shaders/plant.vert",
			"shaders/plant.frag");
		Material material;
		material.setShaderID(shaderId);
		material.setShadingModelId(ShadingModelId::UNLIT);
		material.setUnifValue("baseColor", vec3(0, 0.5, 0.8));
		mesh->setMaterial(material);

		vec2 pos2, tangent2;
		spline->get((float)i / NUM_PLANTS * spline->curveLength(), pos2, tangent2);
		printf("%f %f\n", pos2.x, pos2.y);

		vec3 pos = vec3(pos2.x, -10.5, -pos2.y);
		vec3 tangent = vec3(tangent2.x, 0, -tangent2.y);

		node->setPosition(pos);

		node->setGameObject(mesh);
		root->attachChild(node);
		plants.push_back(node);
	}
}

void renderFunc()
{
	GOMesh* tunnelGo = (GOMesh*)tunnelNode->getGameObject();
	Material tunnelMaterial = tunnelGo->getMaterial();

	// save initial camera configuration
	SceneNode* cameraNode = scene->getActiveCamera()->getSceneNode();
	vec3 originalCameraPos = cameraNode->getPosition();
	vec3 originalCameraRotation = eulerAngles(cameraNode->getRotation());

	// disable the water plane for rendering the reflection and refraction views
	waterPlaneNode->getGameObject()->enable(false);

	// render reflection
	{
		// reflect camera position
		vec3 camPos = originalCameraPos;
		float distToPlane = camPos.y - WATER_HEIGHT;
		camPos.y -= 2 * distToPlane;
		cameraNode->setPosition(camPos);
		// reflect camera pitch
		vec3 camRot = originalCameraRotation;
		camRot.x = -camRot.x;
		cameraNode->setRotation(quat(camRot));

		glEnable(GL_CLIP_DISTANCE0);
		vec4 plane(0, 1, 0, -WATER_HEIGHT);
		tunnelMaterial.setUnifValue("waterPlane", plane);
		tunnelGo->setMaterial(tunnelMaterial);
		for (SceneNode* plant : plants)
		{
			GOMesh* go = (GOMesh*)plant->getGameObject();
			Material mat = go->getMaterial();
			mat.setUnifValue("waterPlane", plane);
			go->setMaterial(mat);
		}

		renderer->renderToCustomTarget(scene, reflectionTarget);

		static bool once = false;
		if (!once)
		{
			//saveTexture("reflect.png", reflectionTarget.getTexture(0));
		}
	}

	// render refraction
	{
		// reset original camera configuration
		cameraNode->setPosition(originalCameraPos);
		cameraNode->setRotation(quat(originalCameraRotation));

		glEnable(GL_CLIP_DISTANCE0);
		vec4 plane(0, -1, 0, WATER_HEIGHT);
		tunnelMaterial.setUnifValue("waterPlane", plane);
		tunnelGo->setMaterial(tunnelMaterial);
		for (SceneNode* plant : plants)
		{
			GOMesh* go = (GOMesh*)plant->getGameObject();
			Material mat = go->getMaterial();
			mat.setUnifValue("waterPlane", plane);
			go->setMaterial(mat);
		}

		renderer->renderToCustomTarget(scene, refractionTarget);

		static bool once = false;
		if (!once)
		{
			//saveTexture("refract.png", refractionTarget.getTexture(0));
		}
	}

	// reenable the water plane
	waterPlaneNode->getGameObject()->enable();

	// render final scene
	{
		// set time
		GOMesh* go = (GOMesh*)waterPlaneNode->getGameObject();
		Material material = go->getMaterial();
		material.setUnifValue("time", globalTime);
		go->setMaterial(material);

		glDisable(GL_CLIP_DISTANCE0);
		renderer->render(scene);
	}

	SDL_GL_SwapWindow(window);
}

void resizeFunc(int width, int height)
{
	printf("resize: %d %d\n", width, height);
	scene->getActiveCamera()->setAspectRatio((float)width/height);
	renderer->resize((unsigned)width, (unsigned)height);
}

void idleFunc(float dt)
{

	// compute fps and set as window title
	const float RECOMP_TIME = 1.f;
	static unsigned frames = 0;
	static float acumTime = 0.f;
	acumTime += dt;
	frames++;
	if (acumTime > RECOMP_TIME)
	{
		float fps = frames / acumTime;
		const unsigned TITLE_MAX_SIZE = 32;
		char title[TITLE_MAX_SIZE];
		snprintf(title, TITLE_MAX_SIZE, "%.2f", fps);
		SDL_SetWindowTitle(window, title);
		acumTime = 0.f;
		frames = 0;
	}

	// camera control
	GOCamera* camera = scene->getActiveCamera();
	SceneNode* cameraNode = camera->getSceneNode();
	glm::vec3 translate(0);
	bool bMove = wPressed || sPressed || aPressed || dPressed;

	if (bMove)
	{
		automaticCamera = false;
		if (wPressed) translate.z = -1;
		else if (sPressed) translate.z = +1;

		if (aPressed) translate.x = -1;
		else if (dPressed) translate.x = +1;

		translate = glm::normalize(translate);
		cameraNode->moveSelf(CAMERA_SPEED * translate * dt);
	}

	if(automaticCamera) cameraMovement(dt);

}

void handleEvent(const SDL_Event& event)
{

	switch (event.type) {

		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_SPACE)
			{
				bool alamb = renderer->isWireframeMode();
				renderer->setWireframeMode(!alamb);
			}
			break;

		/* close button clicked */
		case SDL_QUIT:
			run = false;
			break;

        /* window events (resize)*/
		case SDL_WINDOWEVENT:
		{

			break;
		}
    }

}

void updateKeyboardState()
{
	const Uint8* keyboard = SDL_GetKeyboardState(NULL);
	wPressed = keyboard[SDL_SCANCODE_W];
	aPressed = keyboard[SDL_SCANCODE_A];
	sPressed = keyboard[SDL_SCANCODE_S];
	dPressed = keyboard[SDL_SCANCODE_D];
}

void updateMouseState()
{
    int x, y;
    const Uint32 mouse = SDL_GetMouseState(&x, &y);
    if(mouse & SDL_BUTTON(SDL_BUTTON_LEFT))
    {
        mouseMotionFunc(x, y);
        mousePressed = true;
    }
    else
    {
        mousePressed = false;
    }
	prevMouseX = x;
	prevMouseY = y;
}

void mouseMotionFunc(int x, int y)
{
	const float PITCH_LIMIT = 0.4f*PI;
	const float SENSITIVITY = 0.01f;
	if (mousePressed)
	{
		int dx = x - prevMouseX;
		int dy = y - prevMouseY;
		printf("%d %d\n", dx, dy);

		float fdx = dx * SENSITIVITY;
		float fdy = dy * SENSITIVITY;

		cameraHeading -= fdx;
		normalizeAngle2PI(cameraHeading);
		cameraPitch -= fdy;
		cameraPitch = normalizeAnglePI(cameraPitch);
		cameraPitch = clamp(cameraPitch, -PITCH_LIMIT, PITCH_LIMIT);

		SceneNode* cameraNode = scene->getActiveCamera()->getSceneNode();
		cameraNode->setRotation(glm::quat(glm::vec3(cameraPitch, cameraHeading, 0.f)));
	}
}

void cameraMovement(float dt)
{
	float length = spline->curveLength();
	float totalTime = length / CAMERA_SPEED;

	vec2 pos2;
	vec2 forward2;
	spline->get(length * splineTime / totalTime, pos2, forward2);

	vec3 pos = vec3(pos2.x, -5, -pos2.y);
	
	SceneNode* camera = scene->getActiveCamera()->getSceneNode();
	camera->setPosition(pos);

	quat curRot = camera->getRotation();
	vec3 splineForward = vec3(-forward2.x, 0, -forward2.y);
	quat splineRot = quat_cast(lookAt(vec3(0, 0, 0), splineForward, vec3(0, 1, 0)));

	float blend = dt;

	if (mousePressed)
	{
		quat rot = splineRot;
		camera->setRotation(rot);
	}
	else
	{
		quat rot = slerp(curRot, splineRot, 1.0f);
		camera->setRotation(rot);
	}
	
	cameraPitch = normalizeAnglePI(pitch(camera->getRotation()));
	cameraHeading = normalizeAngle2PI(yaw(camera->getRotation()));

	splineTime += dt;
}